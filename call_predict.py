from cnvrg import Endpoint
import requests
import json
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# scale the values to 0.0 to 1.0
test_images = test_images / 255.0

data = json.dumps({"signature_name": "serving_default", "instances": test_images[0:4].tolist()})
print('Data: {} ... {}'.format(data[:50], data[len(data)-52:]))

e = Endpoint("tfservingdemo")
url = e.url
headers = {"content-type": "application/json"}
json_response = requests.post('{url}/v1/models/fashion_model:predict', data=data, headers=headers)
predictions = json.loads(json_response.text)['predictions']