require 'fileutils'
Dir.chdir ("/input")
Dir.glob('*').each do |task|
  Dir.chdir("#{task}")
  FileUtils.rm_rf %w(.cnvrg .cnvrgignore)
  Dir.chdir "/input"
end
FileUtils.cp_r Dir.glob('*'), "/cnvrg/output"